## CSS
- Content wrap
        
        :::css
        .content-wrap {
          width: 300px;
          margin: 0 auto;
        }


- Overflow
        
        <div class="parent">
          <div class="floated">floated element</div>
        </div> 
    
        .floated {
          float: left;
        }
        .parent {
          overflow: hidden;
          /* OR */
          overflow: auto;
        }
        
  
- [Clearfix](https://css-tricks.com/snippets/css/clear-fix/)


- Background image
        
        :::css
        background: linear-gradient(rgba(141, 153, 174, 0.8), rgba(141, 153, 174, 0.5)), url(../images/toronto.jpg) no-repeat fixed;
        background-size: cover;
    
